<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Berlatih php</title>
</head>
<body>

<?php
function tentukan_nilai($number)
{
    $output ="";
    if($number == 98)
    {
        $output .= "Sangat baik <br>";
    }
    else if($number == 76)
    {
        $output .="Baik <br>";
    }
    else if($number == 67)
    {
        $output .="Cukup <br>";
    }
    else if($number == 43)
    {
        $output .= "Kurang <br>";
    }
    return $output;
}

//TEST CASES
echo tentukan_nilai(98); 
echo tentukan_nilai(76); 
echo tentukan_nilai(67); 
echo tentukan_nilai(43); 
?>



</body>
</html>
